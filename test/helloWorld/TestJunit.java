package helloWorld;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import DemoPrinter.MessageUtil;

public class TestJunit {
	
   String message = "Hello World";	
   MessageUtil messageUtil = new MessageUtil(message);

   @Test
   public void testPrintMessage() {	  
      assertEquals(message,messageUtil.printMessage());
   }
   
   @Test
   public void testPrintEmptyMessage() {
	  message ="";
	  messageUtil = new MessageUtil(message);
      assertEquals(message,messageUtil.printMessage());
   }
   
   @Test
   public void testPrintRepeadedMessage() {
	  message ="Hello World";
	  for (int i=0;i<3000;i++) {
		messageUtil = new MessageUtil(message);
      	assertEquals(message,messageUtil.printMessage());
	  }
   }
}